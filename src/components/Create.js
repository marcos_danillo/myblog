import Component from '../core/component'
import Form from '../core/form'
import Validators from '../core/validators'

export default class Create extends Component {
  constructor(id) {
    super(id)
  }

  init() {
    this.el.addEventListener('submit', onSubmit.bind(this));
    this.form = new Form(this.el, {
      title: [Validators.required],
      fulltext: [Validators.required, Validators.minLength(10)]
    });
  }
}

function onSubmit(evt) {
  evt.preventDefault();

  if(this.form.isValid()){
    const formData = {
      type: this.el.type.value, 
      ...this.form.value()
    }
  
    console.log(formData);
  }else{
    console.warn('Form is invalid!')
  }
  
}