import Component from '../core/component'

export default class Header extends Component {
  constructor(id){
    super(id);
  }

  init() {
    if(!localStorage.getItem('visited')){
      this.show();
    }
    const btn = this.el.querySelector('.js-header-btn');
    btn.addEventListener('click', onHeaderBtnClick.bind(this));
  }
}

function onHeaderBtnClick() {
  localStorage.setItem('visited', true);
  this.hide();
}
