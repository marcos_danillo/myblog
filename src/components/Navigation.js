import Component from '../core/component'

export default class Navigation extends Component {
  constructor(id){
    super(id);

    this.tabs = [];
  }

  init() {
    this.el.addEventListener('click', onTabClick.bind(this));
  }

  registerTabs(tabs){
    this.tabs = tabs;
  }
}

function onTabClick(evt) {
  evt.preventDefault();
  if(evt.target.classList.contains('tab')){
    this.el.querySelectorAll('.tab').forEach((tab) => {
      tab.classList.remove('active');
    });
    evt.target.classList.add('active');
    const activeTab  = this.tabs.find(tab => tab.name === evt.target.dataset.name);
    this.tabs.forEach(tab => tab.component.hide());
    activeTab.component.show();

  }
};