import Header from './components/Header';
import Navigation from './components/Navigation';
import Create from './components/Create';
import Posts from './components/Posts';
import Favorite from './components/Favorite';

new Header('header');
const navigation = new Navigation('navigation');
const create = new Create('create');
const posts = new Posts('posts');
const favorite = new Favorite('favorite');

navigation.registerTabs([
  {name: 'create', component: create},
  {name: 'posts', component: posts},
  {name: 'favorite', component: favorite}
]);
